﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public bool isoMode;
	public Transform target;
	
	public float lerp = 0.125f;
	
	[Header("3thPeron")]
	public Vector3 positionOffset = new Vector3(0, 25, -20);
	
	[Header("IsoMode")]
	public Vector3 rotationOffset = new Vector3(30, 45, 0);
	public float distance = 30;
	
	private void LateUpdate()
	{
		Vector3 targetPos = target.position + positionOffset;
		
		if (isoMode) {
			Quaternion quaternion = Quaternion.Euler (rotationOffset.x, rotationOffset.y, rotationOffset.z);
			transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, lerp);
			targetPos = target.position - (quaternion * Vector3.forward * distance);
		} else {
			transform.LookAt(target);
		}
		
		transform.position = Vector3.Lerp(transform.position, targetPos, lerp);

	}
}
