﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {

	public float speed = 10f;
	public float jumpStrength = 8f;
	public float gravity = 20f;

	private CharacterController controller;
	private Vector3 velocity = Vector3.zero;

	private void Start () {
		controller = GetComponent<CharacterController> ();	
	}
	
	private void Update ()
	{
		if (controller.isGrounded) {
			Move();
			
			if (Input.GetButton("Jump")) {
				Jump();
			}
		}
		
		velocity.y -= gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime);
		Rotate();
	}

	private void Move()
	{
		velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speed;
	}
	
	private void Rotate()
	{
		Vector3 newDir = Vector3.RotateTowards(
			transform.forward,
			new Vector3(velocity.x, 0f, velocity.z),
			speed * Time.deltaTime,
			0.0f);

		transform.rotation = Quaternion.LookRotation(newDir);
	}

	private void Jump()
	{
		velocity.y = jumpStrength;
	}
}
